﻿namespace EcmReports.Clicks.Model
{
    using System;

    public class ClickDataRow
    {
        public Guid ItemId { get; set; }

        public string Url { get; set; }

        public string UrlText { get; set; }

        public int TotalClicks { get; set; }

        public int TotalUserWhoClicked { get; set; }
    }
}