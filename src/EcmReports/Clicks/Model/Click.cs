﻿namespace EcmReports.Clicks.Model
{
    public class Click
    {
        public string Url { get; set; }

        public int TotalClicks { get; set; }

        public int TotalUsersWhoClicked { get; set; }
    }
}