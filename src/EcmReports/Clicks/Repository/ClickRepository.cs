﻿namespace EcmReports.Clicks.Repository
{
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Net;

    using EcmReports.Clicks.Model;

    using Sitecore.Data;
    using Sitecore.Modules.EmailCampaign.Core.Analytics;
    using Sitecore.Modules.EmailCampaign.Speak.Web.Core;

    public class ClickRepository
    {
        #region Constants

        private const int SqlCommandTimeout = 30000;

        #endregion

        #region Fields

        private readonly AnalyticsSqlCommand analyticsSqlCommand;

        private readonly SpeakContext speakContext;

        #endregion

        #region Constructors and Destructors

        public ClickRepository()
            : this(UIFactory.Instance.GetSpeakContext())
        {
        }

        public ClickRepository(SpeakContext speakContext)
        {
            this.speakContext = speakContext;
            var connectionString = ConfigurationManager.ConnectionStrings["analytics"].ConnectionString;
            this.analyticsSqlCommand = AnalyticsFactory.Instance.GetAnalyticsSqlCommand(
                connectionString, 
                SqlCommandTimeout);
        }

        #endregion

        #region Public Methods and Operators

        public List<Click> GetClicks()
        {
            var message = this.speakContext.Message;
            if (message == null || ID.IsNullOrEmpty(message.CampaignId))
            {
                return new List<Click>();
            }
            
            return this.GetClicks(message.CampaignId);
        }

        #endregion

        #region Methods

        private List<Click> GetClicks(ID campaignId)
        {
            const string Query =
                "select  p.ItemId, p.Url,  count(pe.PageEventId), count(distinct v.visitorid) " 
                + "from PageEvents pe "
                + "inner join Pages p on p.PageId = pe.PageId inner join Visits v on v.VisitId = pe.VisitId "
                + "where pe.PageEventDefinitionId = '87431B9B-FA39-4780-BEB3-1047B9E61876' and v.CampaignId = @campaignId "
                + "and p.url NOT LIke '/sitecore/RedirectUrlPage.aspx%' " 
                + "group by  p.ItemId, p.Url " 
                + "union "
                + "select  p.ItemId, substring(p.UrlText, charindex('&ec_url=', p.UrlText)+8, LEN(p.UrlText)),  count(pe.PageEventId), count(distinct v.visitorid) "
                + "from PageEvents pe "
                + "inner join Pages p on p.PageId = pe.PageId inner join Visits v on v.VisitId = pe.VisitId "
                + "where pe.PageEventDefinitionId = '87431B9B-FA39-4780-BEB3-1047B9E61876' and v.CampaignId = @campaignId "
                + "and p.url LIke '/sitecore/RedirectUrlPage.aspx%' "
                + "group by  p.ItemId, substring(p.UrlText, charindex('&ec_url=', p.UrlText)+8, LEN(p.UrlText)) ";

            var clickDataRows = this.analyticsSqlCommand.ExecuteCommand(
                this.MapToClickDataRow,
                Query,
                new CommandParameter("@campaignId", campaignId.ToGuid()));

            return this.ProcessClicks(clickDataRows);
        }

        private List<Click> ProcessClicks(IEnumerable<ClickDataRow> clickDataRows)
        {
            return
                clickDataRows.Select(
                    row =>
                    new Click
                        {
                            Url = row.Url.StartsWith("http") ? WebUtility.UrlDecode(row.Url) : row.Url,
                            TotalClicks = row.TotalClicks,
                            TotalUsersWhoClicked = row.TotalUserWhoClicked
                        }).OrderBy(click => click.Url).ToList();
        }

        private ClickDataRow MapToClickDataRow(IDataReader reader)
        {
            return new ClickDataRow
                       {
                           ItemId = reader.GetGuid(0),
                           Url = reader.GetString(1),
                           TotalClicks = reader.GetInt32(2),
                           TotalUserWhoClicked = reader.GetInt32(3)
                       };
        }

        #endregion
    }
}